/**
 * 
 */
package project.toy.robert.game.ToyRobert;

import org.junit.Test;

import amdocs.toy.robert.game.location.Direction;
import amdocs.toy.robert.game.location.Position;
import amdocs.toy.robert.game.operation.LeftCommand;
import amdocs.toy.robert.game.operation.MoveCommand;
import amdocs.toy.robert.game.operation.PlaceCommand;
import amdocs.toy.robert.game.operation.ReportCommand;
import junit.framework.TestCase;

/**
 * @author evichaw
 *
 */
public class MainTest extends TestCase {

	/**
	 * @param name
	 */
	public MainTest(String name) {
		super(name);
	}

	/* (non-Javadoc)
	 * @see junit.framework.TestCase#setUp()
	 */
	protected void setUp() throws Exception {
		super.setUp();
	}

	/* (non-Javadoc)
	 * @see junit.framework.TestCase#tearDown()
	 */
	protected void tearDown() throws Exception {
		super.tearDown();
	}
	/*
	 *  a)
PLACE 0,0,NORTH
MOVE
REPORT

Output: 0,1,NORTH
*/
	@Test
	public void testscenario1() {
		Position position = new Position(0, 0, Direction.NORTH);
		ReportCommand report = new ReportCommand(position);
		PlaceCommand palce = new PlaceCommand();
		palce.process(position, report);
		MoveCommand move = new MoveCommand();
		move.process(position, report);
		System.out.println(report.process(position));
		assertTrue(report.process(position).length()>0);
		
	}
/*
 * b)----------------
PLACE 0,0,NORTH
LEFT
REPORT

Output: 0,0,WEST
 */
	@Test
	public void testscenario2() {
		Position position = new Position(0, 0, Direction.NORTH);
		ReportCommand report = new ReportCommand(position);
		PlaceCommand palce = new PlaceCommand();
		palce.process(position, report);
		LeftCommand left = new LeftCommand();
		left.process(position, report);
		System.out.println(report.process(position));
		assertTrue(report.process(position).length()>0);
		
	}
	/*
	 * c)----------------
PLACE 1,2,EAST
MOVE
MOVE
LEFT
MOVE
REPORT
	 */
	@Test
	public void testscenario3() {
		Position position = new Position(1, 2, Direction.EAST);
		ReportCommand report = new ReportCommand(position);
		PlaceCommand palce = new PlaceCommand();
		palce.process(position, report);
		LeftCommand left = new LeftCommand();
		MoveCommand move = new MoveCommand();
		move.process(position, report);
		move.process(position, report);
		left.process(position, report);
		move.process(position, report);
		System.out.println(report.process(position));
		assertTrue(report.process(position).length()>0);
		
	}
}

