package amdocs.toy.robert.game.Logger;

import org.apache.log4j.Logger;


public class Logging {
	private static Logger LOG = null; 
	private Logging(){
		
	}
	
	public static Logger getInstance() {
		if (LOG == null) 
			LOG = Logger.getLogger(Logging.class);
  
        return LOG; 
    } 
	
	
}
