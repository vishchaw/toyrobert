package amdocs.toy.robert.game.ToyRobert;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;


import amdocs.toy.robert.game.Logger.Logging;
import amdocs.toy.robert.game.location.Commands;
import amdocs.toy.robert.game.location.Direction;
import amdocs.toy.robert.game.location.Position;
import amdocs.toy.robert.game.operation.LeftCommand;
import amdocs.toy.robert.game.operation.MoveCommand;
import amdocs.toy.robert.game.operation.PlaceCommand;
import amdocs.toy.robert.game.operation.ReportCommand;
import amdocs.toy.robert.game.operation.RightCommand;


public class CommandExecution {
	 Direction direc ;
	 Commands cmd;
	 BufferedReader reader =  new BufferedReader(new InputStreamReader(System.in));
	 String inputString = null;
	 
	Position position = null;
	ReportCommand report = null;
	PlaceCommand place = new PlaceCommand();
	MoveCommand move = new MoveCommand();
	LeftCommand left = new LeftCommand();
	RightCommand right = new RightCommand(); 
	int count =0;
	boolean executionFlag = true;
	public void process() {
		Logging.getInstance().info("Command Execution: Process()");
	while(executionFlag) {
		try {
			inputString = reader.readLine();
			Logging.getInstance().info("String reading from the console --> "+inputString);
		} catch (IOException e) {
			Logging.getInstance().error("Inside IO exception: No input provided"); 
			System.err.println("Inside IO exception: No input provided");
		}	
	if(inputString.equalsIgnoreCase("QUIT")) {
		
		Logging.getInstance().info("Quit !!!!");
		System.out.println("BYE BYE !!!!!!");
		executionFlag=false;
	}
	else if(inputString.contains("PLACE")) {
		Logging.getInstance().info("Place command entering !!!");
		String str[] = inputString.split(",");
		if(str.length<4||str.length>4) {
		System.out.println("Invalid input !!! Please enter the PLACE command in below format\n\tPLACE,X,Y,<DIRECTION>\n");	
		}
		else{
		try {	
		initDirection(str[3]);
		position = new Position(Integer.parseInt(str[1]), Integer.parseInt(str[2]), direc);
		report = new ReportCommand(position);
		place.process(position, report);
		count ++;
		}
		catch(NumberFormatException e) {
			Logging.getInstance().error("Input is not in number format: incorrect input provided " + e.getMessage()); 
			System.err.println("Input is not in number format: incorrect input provided " + e.getMessage());
		}
		}
	}
	else {
		if(count<1) {
			System.out.println("Please initialize the postion by PLACE command!!\n");
		
		}
		else {
			if(inputString.equalsIgnoreCase("MOVE")) {
				Logging.getInstance().info("Move Command");
				move.process(position, report);
			}
			else if(inputString.equalsIgnoreCase("LEFT")) {
				Logging.getInstance().info("Left Command");
				left.process(position, report);
			}
			else if(inputString.equalsIgnoreCase("RIGHT")) {
				Logging.getInstance().info("Right Command");
				right.process(position, report);
			}
			else if(inputString.equalsIgnoreCase("REPORT")) 
			{
				Logging.getInstance().info("Reporting");
				report = new ReportCommand(position);
				System.out.println(report.process(position));
				
				
			}
			else {
				System.out.println("-------Command not valid !! Enter the valid command -----");
			}
		}
		
	}
	}
	
}

public  void initDirection(String direction) {
	if(direction.equalsIgnoreCase("NORTH")) {
	 direc = Direction.NORTH;
	}
	else if(direction.equalsIgnoreCase("SOUTH")) {
   	 direc = Direction.SOUTH;
   	}
   	else if(direction.equalsIgnoreCase("WEST")) {
   	 direc = Direction.WEST;
   	}
   	else if(direction.equalsIgnoreCase("EAST")) {
   	 direc = Direction.EAST;
   	}
   	
 }

	
}

