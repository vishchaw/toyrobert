package amdocs.toy.robert.game.ToyRobert;


import amdocs.toy.robert.game.Logger.Logging;

/**
 * TOY Robert by Vishal Chawla.
 * This is console based working example and i have used buffer reader for the readline
 * Kindly enter the command on console and operation will be performed.
 *
 */
public class Main 
{ 
	//private static final Logger LOG =  Logger.getLogger(Main.class);
    public static void main( String[] args )
    {
    	Logging.getInstance().info("Hello Inside the Toy Rebert Game !!!!"); 
    	System.out.println( "Hello Inside the Toy Rebert Game !!!!" );
    	System.out.println("Please enter a valid command from below list :");
    	System.out.println("1.\tPLACE,X,Y,NORTH|SOUTH|WEST|EAST\n2.\tMOVE\n3.\tLEFT\n4.\tRIGHT\n5.\tREPORT\n6.\tQUIT");
    	try {
    	CommandExecution exc = new CommandExecution();
    	exc.process();
    	}
    	catch(Exception e) {
    		Logging.getInstance().error("Exception occured Inside the Toy Rebert Game !!!! "+e.getMessage());
    		System.err.println("Excepotion Occured "+e.getMessage());
    	}
    }
    
    
}

