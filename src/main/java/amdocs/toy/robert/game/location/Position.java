package amdocs.toy.robert.game.location;

/**
 * @author Vishal Chawla
 *
 */
public class Position {
	 public static final Integer MAX_POSITION = 4;
	    public static final Integer MIN_POSITION = 0;

	    private Integer xPosition;

	    private Integer yPosition;

	    private Direction Direction;

	   

	    public Position(Integer xPosition, Integer yPosition, Direction Direction) {
	        this.xPosition = xPosition;
	        this.yPosition = yPosition;
	        this.Direction = Direction;
	    }

	    public Integer getXPosition() {
	        return xPosition;
	    }

	    @Override
		public String toString() {
			return "Position [xPosition=" + xPosition + ", yPosition=" + yPosition + ", Direction=" + Direction + "]";
		}

		public void setXPosition(Integer xPosition) {
	        this.xPosition = xPosition;
	    }

	    public Integer getYPosition() {
	        return yPosition;
	    }

	    public void setYPosition(Integer yPosition) {
	        this.yPosition = yPosition;
	    }

	    public Direction getDirection() {
	        return Direction;
	    }

	    public void setDirection(Direction Direction) {
	        this.Direction = Direction;
	    }

	    public boolean isOnTable() {
	        return xPosition != null && yPosition != null && Direction != null;
	    }

	    public String getCurrentStatus() {
	        return String.join(",", xPosition.toString(), yPosition.toString(), Direction.toString());
	    }


	    public void increaseYPosition() {
	        yPosition++;
	    }

	    public void decreaseYPosition() {
	        yPosition--;
	    }

	    public void increaseXPosition() {
	        xPosition++;
	    }

	    public void decreaseXPosition() {
	    	xPosition--;
	    }
}
