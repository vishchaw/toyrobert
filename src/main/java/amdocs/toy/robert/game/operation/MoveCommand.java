package amdocs.toy.robert.game.operation;

import amdocs.toy.robert.game.location.Position;

public class MoveCommand extends command {
	
	public void process(Position position,ReportCommand report) {
	switch (position.getDirection()) {
    case NORTH:
        if (position.getYPosition() < Position.MAX_POSITION) {
            position.increaseYPosition();
       } 
        break;

    case SOUTH:
        if (position.getYPosition() > Position.MIN_POSITION) {
            position.decreaseYPosition();
          } 
        break;

    case EAST:
        if (position.getXPosition() < Position.MAX_POSITION) {
            position.increaseXPosition();
           
        } 
        break;

    case WEST:
        if (position.getXPosition() > Position.MIN_POSITION) {
            position.decreaseXPosition();
          
        } 
        break;
}
	System.out.println(report.process(position));
	}
}

