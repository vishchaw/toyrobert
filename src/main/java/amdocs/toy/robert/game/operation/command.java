package amdocs.toy.robert.game.operation;

import amdocs.toy.robert.game.location.Position;

public class command {

	public abstract class Command {

	    public abstract void process(Position position, ReportCommand report);
	} 
}
