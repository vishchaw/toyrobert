package amdocs.toy.robert.game.operation;

import amdocs.toy.robert.game.Logger.Logging;
import amdocs.toy.robert.game.location.Direction;
import amdocs.toy.robert.game.location.Position;

public class LeftCommand extends command {

	
	public void process(Position position,ReportCommand report) {
		
	switch (position.getDirection()) {
         case NORTH:
        	 Logging.getInstance().info("Moving towards west");
        	 position.setDirection(Direction.WEST);
             break;
         case SOUTH:
        	 Logging.getInstance().info("Moving towards east");
        	 position.setDirection(Direction.EAST);
             break;
         case EAST:
        	 Logging.getInstance().info("Moving towards north");
        	 position.setDirection(Direction.NORTH);
             break;
         case WEST:
        	 Logging.getInstance().info("Moving towards south");
        	 position.setDirection(Direction.SOUTH);
             break;
	}
	System.out.println(report.process(position));
}
}