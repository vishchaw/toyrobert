package amdocs.toy.robert.game.operation;

import amdocs.toy.robert.game.location.Direction;
import amdocs.toy.robert.game.location.Position;

public class RightCommand extends command {

	public void process(Position position,ReportCommand report) {
		switch (position.getDirection()) {
        case NORTH:
            position.setDirection(Direction.EAST);
            break;
        case SOUTH:
            position.setDirection(Direction.WEST);
            break;
        case EAST:
            position.setDirection(Direction.SOUTH);
            break;
        case WEST:
            position.setDirection(Direction.NORTH);
            break;
	}
		System.out.println(report.process(position));
}
}
