package amdocs.toy.robert.game.operation;

import amdocs.toy.robert.game.location.Direction;
import amdocs.toy.robert.game.location.Position;

public class PlaceCommand extends command {

	int xAxis,yAxis;
	Direction fAxis;
	String command;
	
	public void process(Position position,ReportCommand report) {
		
		this.xAxis = position.getXPosition();
    	this.yAxis = position.getYPosition();
    	this.fAxis = position.getDirection();
		if(xAxis<Position.MAX_POSITION&&xAxis>Position.MIN_POSITION&&yAxis<Position.MAX_POSITION&&yAxis>Position.MIN_POSITION) {
			position.setDirection(fAxis);
			position.setXPosition(xAxis);
			position.setYPosition(yAxis);
		}
		System.out.println(report.process(position));
	}
}

