package amdocs.toy.robert.game.operation;

import amdocs.toy.robert.game.location.Position;

public class ReportCommand  {

	Position position = null;
	
	
	public ReportCommand(Position position) {
		super();
		this.position = position;
	}
	public String process(Position position) {
		return position.getCurrentStatus();
	}
}
